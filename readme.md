# CPPN WGAN for MNIST Generation

## Training the model 

```

python wgpan_gp.py 
```


## Random generate image samples        

Use the pretrained model to generate 8 images of size 512 x 512 

Usage:       

    python sample.py sample CKPT SIZE [BATCH_SIZE] [LATENT_DIM]
    python sample.py sample --ckpt CKPT --size SIZE [--batch-size BATCH_SIZE] [--latent-dim LATENT_DIM]


```
python sample.py sample trained_models/mnist/g-cppn-wgan_iter_195.pth 512 8
```

## Continuously sampling from the latent space 

Use the pretrained model to sample from the continuous latent space and the 
generated samples would be saved as a video to show the transformation of the generation results.

Usage:       

    python sample.py interpolate CKPT SIZE [STEPS] [BATCH_SIZE] [LATENT_DIM] [TARGET_DIM]
    python sample.py interpolate --ckpt CKPT --size SIZE [--steps STEPS] [--batch-size BATCH_SIZE] [--latent-dim LATENT_DIM] [--target-dim TARGET_DIM]


```
python  sample.py interpolate trained_models/mnist/g-cppn-wgan_iter_195.pth 512 64 16
```


## References
cppn-art <https://github.com/johnguibas/cppn-art>
PyTorch-GAN <https://github.com/eriklindernoren/PyTorch-GAN>
