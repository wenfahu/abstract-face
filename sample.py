import fire
import torch
import numpy as np
from torch.distributions.normal import Normal
from Cppn import CPPN, get_coordinates
from torchvision.utils import save_image
import os
import os.path as osp

mnist_img_size = 28
device = 'cuda' if torch.cuda.is_available() else 'cpu'
normal_dist = Normal(0, 1)
torch.no_grad()

def sample(ckpt, size, batch_size=32, latent_dim=128):
    generator = CPPN(mnist_img_size, mnist_img_size, latent_dim=latent_dim, out_channel=1)
    generator.to(device)
    state_dict = torch.load(ckpt)
    generator.load_state_dict(state_dict)
    x, y, r = get_coordinates(size, size, batch_size=batch_size)

    noise = normal_dist.sample(torch.Size((batch_size, latent_dim)))
    ones = torch.ones(batch_size, size * size, 1)
    z = torch.bmm(ones, noise.unsqueeze(1))

    x, y, r, z = x.to(device), y.to(device), r.to(device), z.to(device)

    fake_images = generator(z, x, y, r)  
    file_name = 'images/{}_generated_{}x{}.png'.format(osp.splitext(osp.basename(ckpt))[0], size, size)
    save_image(fake_images, file_name, nrow=4, normalize=True)

def interpolate(ckpt, size, steps=32, batch_size=32, latent_dim=128, target_dim=64):
    generator = CPPN(mnist_img_size, mnist_img_size, latent_dim=latent_dim, out_channel=1)
    generator.to(device)
    state_dict = torch.load(ckpt)
    generator.load_state_dict(state_dict)

    noise = normal_dist.sample(torch.Size((1, latent_dim))).repeat(steps, 1)
    samples = torch.linspace(-10, 10, steps)
    noise[:, target_dim] = samples

    ones = torch.ones(steps, size * size, 1)
    z = torch.bmm(ones, noise.unsqueeze(1))

    fake_images = []
    for z_batch in torch.split(z, batch_size):
        x, y, r = get_coordinates(size, size, batch_size=len(z_batch))
        x, y, r, z_batch = x.to(device), y.to(device), r.to(device), z_batch.to(device)

        fake_images.append(generator(z_batch, x, y, r).detach().cpu() )
    fake_images = torch.cat(fake_images) 
    import imageio
    if not osp.isdir('./videos'):
        os.makedirs('./videos')
    writer = imageio.get_writer('./videos/interpolation_{}_{}x{}.mp4'.format(
        osp.splitext(osp.basename(ckpt))[0], size, size), codec='h264')
    for frame in fake_images:
        ndarray = frame.mul(255).clamp(0, 255).byte().permute(1, 2, 0).numpy()
        writer.append_data(ndarray)

if __name__ == "__main__":
    fire.Fire()

