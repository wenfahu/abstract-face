import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import numpy as np

def get_coordinates(x_dim = 28, y_dim = 28, scale = 8, batch_size = 1):
    '''
    calculates and returns a vector of x and y coordintes, and corresponding radius from
    the centre of image.
    '''
    n_points = x_dim * y_dim
    # creates a list of x_dim values ranging from -1 to 1, then scales them by scale
    x_range = scale*(np.arange(x_dim)-(x_dim-1)/2.0)/(x_dim-1)/0.5
    y_range = scale*(np.arange(y_dim)-(y_dim-1)/2.0)/(y_dim-1)/0.5
    x_mat = np.matmul(np.ones((y_dim, 1)), x_range.reshape((1, x_dim)))
    y_mat = np.matmul(y_range.reshape((y_dim, 1)), np.ones((1, x_dim)))
    r_mat = np.sqrt(x_mat*x_mat + y_mat*y_mat)
    x_mat = np.tile(x_mat.flatten(), batch_size).reshape(batch_size, n_points, 1)
    y_mat = np.tile(y_mat.flatten(), batch_size).reshape(batch_size, n_points, 1)
    r_mat = np.tile(r_mat.flatten(), batch_size).reshape(batch_size, n_points, 1)
    return torch.from_numpy(x_mat).float(), torch.from_numpy(y_mat).float(), torch.from_numpy(r_mat).float()


def coordinates(x_dim, y_dim, scale=8.0, batch_size=32):
    n_pixel = x_dim * y_dim
    x_range = scale * (np.arange(x_dim) - (x_dim-1)/2.0)/(x_dim-1)/0.5
    y_range = scale * (np.arange(y_dim) - (y_dim-1)/2.0)/(y_dim-1)/0.5
    x_mat = np.matmul(np.ones((y_dim, 1)), x_range.reshape((1, x_dim)))
    y_mat = np.matmul(y_range.reshape((y_dim, 1)), np.ones((1, x_dim)))
    r_mat = np.sqrt(x_mat*x_mat + y_mat*y_mat)
    x_mat = np.tile(x_mat.flatten(), batch_size).reshape(batch_size, n_pixel, 1)
    y_mat = np.tile(y_mat.flatten(), batch_size).reshape(batch_size, n_pixel, 1)
    r_mat = np.tile(r_mat.flatten(), batch_size).reshape(batch_size, n_pixel, 1)
    return x_mat, y_mat, r_mat


class CPPN(nn.Module):
    def __init__(self, x_dim, y_dim, scale=8.0, latent_dim=32, out_channel=3):

        super(CPPN, self).__init__()
        self.gen_x_dim = x_dim
        self.gen_y_dim = y_dim
        self.num_points = x_dim * y_dim
        self.scale = scale
        self.latent_dim = latent_dim

        self.linear_x0 = nn.Linear(1, 128, bias=False)
        self.linear_y0 = nn.Linear(1, 128, bias=False)
        self.linear_r0 = nn.Linear(1, 128, bias=False)
        self.linear_z0 = nn.Linear(self.latent_dim, 128)

        def block(in_feat, out_feat):
            layers = [nn.Linear(in_feat, out_feat)]
            layers.append(nn.Tanh())
            return layers

        self.main_block = nn.Sequential(
                *block(128, 128),
                *block(128, 128),
                *block(128, 128),
                )

        self.fc = nn.Linear(128, out_channel) 
        self.out_channel = out_channel

    def forward(self, z, x, y, r):
        '''
        z of shape batch_size x latent dim

        '''
        batch_size = x.size(0)
        # z_scaled = z.unsqueeze(1) * torch.ones((self.num_points, 1), 
        #         dtype=torch.float32).cuda() * self.scale
        # z_unroll = z_scaled.view(batch_size*self.num_points, z.size(-1))
        # x_unroll = x.view(batch_size * self.num_points, 1)
        # y_unroll = y.view(batch_size * self.num_points, 1)
        # r_unroll = r.view(batch_size * self.num_points, 1)

        U = self.linear_x0(x) + self.linear_y0(y) \
                + self.linear_r0(r) + self.linear_z0(z)

        U = F.softplus(U)
        U = self.main_block(U)
        U = self.fc(U)
        U = torch.sigmoid(U)
        U = U.squeeze(2).view(x.size(0), int(math.sqrt(x.size(1))), int(math.sqrt(x.size(1)))).unsqueeze(1)
        U = U.view(-1, self.out_channel, U.size(2), U.size(3))
        return U

if __name__ == "__main__":
    from torch.distributions.normal import Normal
    import matplotlib.pyplot as plt
    n = Normal(0, 1)
    z = n.sample(torch.Size([32, 32]))
    x, y, r = coordinates(32, 32)
    x, y, r = torch.from_numpy(x).type(torch.float32),\
            torch.from_numpy(y).type(torch.float32),\
            torch.from_numpy(r).type(torch.float32)
    m = CPPN(32, 32)

    U = m(z, x, y, r)
    print (U.shape)

    fig = plt.figure()
    for i in range(1, 33):
        plt.subplot(4, 8, i)
        plt.imshow(U[i-1].data.numpy())

    plt.show()

